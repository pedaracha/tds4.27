// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FunctionLib/Types.h"
#include "TPSCharacter.generated.h"

UCLASS(Blueprintable)
class ATPSCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATPSCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;
	
	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

	UFUNCTION()
	void InputAxisX(float value);
	UFUNCTION()
	void InputAxisY(float X);

	float AxisX=0.0f;
	float AxisY=0.0f;

	UFUNCTION()
	void MovementTick(float DeltaTime);
	
	UFUNCTION()
	void CharacterUpdate();
	
	//UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Movement")
	EMovementState MovementState=EMovementState::Run_State;	
public:

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Movement")
	FCharacterSpeed MovementInfo;
	

	UFUNCTION(BlueprintCallable)
	void ChangeMovementState(EMovementState NewMovementState);
	
	UFUNCTION(BlueprintCallable)
	EMovementState GetMovementState();
};

