
#pragma once

#include  "Kismet/BlueprintFunctionLibrary.h"
#include  "Types.generated.h"

///states of movement on the order of priority
UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Sprint_State UMETA(DisplayName = "Sprint State"),
	Walk_State UMETA(DisplayName = "Walk State"),
	Aim_State UMETA(DisplayName = "AimState"),
	Run_State UMETA(DisplayName = "Run State"),
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Movement")
	float AimSpeed=300.0;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Movement")
	float SprintSpeed = 800.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Movement")
	float WalkSpeed = 200.0;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Movement")
	float RunSpeed = 600.0f;
};

UCLASS()
class TPS_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
};
